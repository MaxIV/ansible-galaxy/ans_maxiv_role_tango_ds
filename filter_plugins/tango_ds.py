from collections import namedtuple
from ansible.errors import AnsibleFilterError

DSKey = namedtuple("DSKey", ["name", "separator", "suffix"])
CONDA_DEFAULT_DEPENDENCIES = ("python", "pytango", "cpptango")


def tango_ds_devices(tango_ds):
    """Filter to return the tango_devices definition from the tango_ds list"""
    tango_devices = []
    for server in tango_ds:
        for instance in server.get("instances", []):
            if instance.get("state", "") == "absent":
                continue
            for device in instance.get("devices", []):
                # Copy all key/value except name (to be replaced by device)
                tango_device = {
                    key: value for key, value in device.items() if key != "name"
                }
                tango_device["device"] = device["name"]
                tango_device["server"] = server["name"]
                tango_device["instance"] = instance["name"]
                if "class" not in tango_device:
                    # Use server name as default class
                    tango_device["class"] = server["name"]
                tango_devices.append(tango_device)
    return tango_devices


def tango_ds_instances(tango_ds):
    """Filter to return the instances (starter servers) from the tango_ds list"""
    ds_instances = []
    for server in tango_ds:
        for instance in server.get("instances", []):
            ds_instance = {
                "server": f"{server['name']}/{instance['name']}",
                "state": instance.get("state", "started"),
            }
            if "level" in instance:
                ds_instance["level"] = instance["level"]

            ds_instances.append(ds_instance)
    return ds_instances


def parse_ds_server(server, versions, *ds_keys):
    ds_server = {key: value for key, value in server.items() if key != "instances"}
    for ds_key in ds_keys:
        ds_server.setdefault(ds_key.name + ds_key.suffix, [])
        if ds_key.name in server:
            for name, version in server[ds_key.name].items():
                if version == "latest":
                    raise AnsibleFilterError(
                        'Using "latest" isn\'t supported. Please specify a version or "default" for {}'.format(
                            name
                        )
                    )
                if version == "default":
                    package = name + ds_key.separator + str(versions[name])
                else:
                    package = name + ds_key.separator + str(version)
                ds_server[ds_key.name + ds_key.suffix].append(package)
    # servers needed to restart when the packages change
    ds_server["servers"] = [
        f"{server['name']}/{instance['name']}"
        for instance in server.get("instances", [])
        if instance.get("state", "started") == "started"
    ]
    # Set the state to absent only if all instances are set to absent
    instances_state = [
        instance.get("state", "present") == "absent"
        for instance in server.get("instances", [])
    ]
    if instances_state and all(instances_state):
        ds_server["state"] = "absent"
    else:
        ds_server["state"] = "present"
    return ds_server


def update_conda_default_dependencies(ds_server, versions):
    """Add the conda default dependencies if not already defined"""
    for pkg in CONDA_DEFAULT_DEPENDENCIES:
        if pkg not in ds_server.get("conda_packages", []):
            ds_server["conda_packages_list"].append(f"{pkg}={versions[pkg]}")


def tango_ds_conda_servers(tango_ds, versions):
    """Filter to return the conda servers from the tango_ds list"""
    ds_servers = []
    for server in tango_ds:
        if "packages_stable" in server or "packages_testing" in server:
            if "conda_packages" in server or "pip_packages" in server:
                raise AnsibleFilterError(
                    "conda_packages/pip_packages are mutually exclusive with packages_stable/packages_testing"
                )
            # Skip servers installed with RPM
            continue
        ds_server = parse_ds_server(
            server,
            versions,
            DSKey("conda_packages", "=", "_list"),
            DSKey("pip_packages", "==", "_list"),
        )
        if "conda_env_name" not in ds_server:
            ds_server["conda_env_name"] = server["name"]
        if "channels" not in ds_server:
            ds_server["channels"] = []
        if "pypi_repo" not in ds_server:
            ds_server["pypi_repo"] = "pypi-virtual-stable"
        update_conda_default_dependencies(ds_server, versions)
        ds_servers.append(ds_server)
    return ds_servers


def update_rpm_servers(ds_servers):
    # As RPM are installed globally, if one is defined for several servers,
    # it will be installed/updated only once but all instances shall be restarted.
    # -> generate the list of servers with common RPM
    stable_rpm_servers = [
        (set(ds_server["packages_stable_present"]), ds_server["servers"])
        for ds_server in ds_servers
    ]
    testing_rpm_servers = [
        (set(ds_server["packages_testing_present"]), ds_server["servers"])
        for ds_server in ds_servers
    ]
    for ds_server in ds_servers:
        ds_servers_set = set(ds_server["servers"])
        for key, l in (
            ("packages_stable_present", stable_rpm_servers),
            ("packages_testing_present", testing_rpm_servers),
        ):
            for rpms, servers in l:
                if set(ds_server[key]).intersection(rpms):
                    ds_servers_set.update(servers)
        ds_server["servers"] = sorted(list(ds_servers_set))


def tango_ds_rpm_servers(tango_ds, versions):
    """Filter to return the RPM servers from the tango_ds list"""
    ds_servers = []
    for server in tango_ds:
        if "conda_packages" in server or "pip_packages" in server:
            if "packages_stable" in server or "packages_testing" in server:
                raise AnsibleFilterError(
                    "conda_packages/pip_packages are mutually exclusive with packages_stable/packages_testing"
                )
            # Skip servers installed with conda
            continue
        if "packages_stable" not in server and "packages_testing" not in server:
            # No packages specified - this can be valid if state is absent
            # We assume conda in that case - no cleaning to do for RPM
            continue
        ds_server = parse_ds_server(
            server,
            versions,
            DSKey("packages_stable", "-", "_present"),
            DSKey("packages_testing", "-", "_present"),
        )
        ds_servers.append(ds_server)
    update_rpm_servers(ds_servers)
    return ds_servers


def tango_ds_get_entry_points(results):
    """Filter to parse the result from the get entry points command"""
    wrappers = []
    for result in results:
        if "stdout_lines" in result:
            for entry_point in result["stdout_lines"]:
                wrappers.append(
                    {
                        "conda_env_name": result["ds"]["conda_env_name"],
                        "name": entry_point,
                    }
                )
    return wrappers


class FilterModule(object):
    """Ansible tango ds jinja2 filters"""

    def filters(self):
        filters = {
            "tango_ds_devices": tango_ds_devices,
            "tango_ds_instances": tango_ds_instances,
            "tango_ds_conda_servers": tango_ds_conda_servers,
            "tango_ds_rpm_servers": tango_ds_rpm_servers,
            "tango_ds_get_entry_points": tango_ds_get_entry_points,
        }
        return filters
