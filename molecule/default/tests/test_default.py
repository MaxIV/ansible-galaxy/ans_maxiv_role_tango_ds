import json
import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("molecule_group")


def test_ds_running(host):
    processes = host.process.filter(user="tangosys")
    if host.ansible.get_variables()["inventory_hostname"] == "tango-ds-default":
        # Only Starter running
        assert len(processes) == 1
        assert processes[0].comm == "Starter"
    else:
        names = {p.comm for p in processes}
        # Python might be listed - check for superset and not equality
        assert names.issuperset(
            {
                "Starter",
                "Achtung",
                "Basler",
                "CosaxsPandabox",
                "VacuumGauge",
                "WaterPump",
            }
        )
        # Keep only 2 last arguments of the command to remove python or bash
        # /bin/bash /usr/local/bin/Achtung 2
        # /usr/bin/python3.6 /usr/bin/WaterPump BXXXA
        commands = {" ".join(p.args.split(" ")[-2:]) for p in processes}
        assert commands.issuperset(
            {
                "/usr/bin/VacuumGauge BXXXA",
                "/usr/bin/WaterPump BXXXA",
                "/usr/bin/Basler FE-SCRN-01",
                "/usr/bin/Basler O-SCRN-01",
                "/opt/conda/envs/CosaxsPandabox/bin/CosaxsPandabox B310A",
                "/opt/conda/envs/achtung-env/bin/Achtung 1",
                "/opt/conda/envs/achtung-env/bin/Achtung 2",
            }
        )
        # O-SCRN-02 state set to "present" - it shouldn't be started
        assert "/usr/bin/Basler O-SCRN-02" not in commands


def test_conda_env(host):
    cmd = host.run("/opt/conda/bin/conda env list --json")
    envs = json.loads(cmd.stdout)["envs"]
    if host.ansible.get_variables()["inventory_hostname"] == "tango-ds-default":
        assert envs == ["/opt/conda"]
    else:
        assert set(envs) == {
            "/opt/conda",
            "/opt/conda/envs/CosaxsPandabox",
            "/opt/conda/envs/achtung-env",
            "/opt/conda/envs/highleveldevices",
        }
        # Check some highleveldevices entry points wrappers
        for name in ("BeamShutter", "CirculationFan", "LinearActuator", "WaterPump"):
            assert host.file(f"/usr/local/bin/{name}").contains(
                f"/opt/conda/envs/highleveldevices/bin/{name}"
            )
        # No wrapper for "highleveldevices" itself
        assert not host.file("/usr/local/bin/highleveldevices").exists


def test_starter(host):
    if host.ansible.get_variables()["inventory_hostname"] != "tango-ds-default":
        cmd = host.run(
            f'python3 -c "import tango; print('
            f"tango.DeviceProxy('tango/admin/{host.ansible.get_variables()['inventory_hostname']}')"
            f".read_attribute('Servers').value)\""
        )
        output = cmd.stdout.replace("\\t", " ")

        # struct [name] [state] [controlled] [level] [nb instances]
        assert "Achtung/1 ON 1 4 1" in output
        assert "Basler/FE-SCRN-01 ON 1 5 1" in output
        assert "O-SCRN-02" not in output
