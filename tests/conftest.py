import pytest
import yaml


TANGO_DS = """
   - name: TangoTest
     channels:
       - maxiv-kits-dev
     conda_packages:
       tango-test: default
     instances:
       - name: test
         devices:
           - name: some-device/test/1
             properties:
               Foo: Bar
               Baz: 42
           - name: some-device/test/2
   - name: Achtung
     conda_env_name: achtung-env
     pip_packages:
       tangods-achtung: 0.1.5
     instances:
       - name: 1
         devices:
           - name: test/achtung/1
       - name: 2
         devices:
           - name: test/achtung/2
             class: Achtung
   - name: Basler
     packages_stable:
       tangods-basler: default
     instances:
       - name: FE-SCRN-01
         devices:
           - name: BASLER/FE-SCRN-01/main
             properties:
               camera_ip: "b303a-fe-dia-cam-01.maxiv.lu.se"
               inter_packet_delay: "4000"
               packet_size: "1500"
               max_push_event_frequency: '2'
       - name: O-SCRN-01
         state: present
         devices:
           - name: BASLER/O-SCRN-01/main
             properties:
               camera_ip: "b303a-o-dia-scrn-01.maxiv.lu.se"
               inter_packet_delay: "4000"
               packet_size: "1500"
               max_push_event_frequency: '2'
   - name: Uview
     instances:
       - name: BXXXA
         state: absent
"""

VERSIONS = """
versions:
  python: 3.9
  pytango: 9.3.3
  cpptango: 9.3.5
  foo: 1.2.1
  bar: 3.0.0
  tangods-basler: 1.1.1
  tangods-uview: 0.5.0
  tangods-achtung: 0.2.0
  tango-test: 3.4
  package2: 2.0.0
  tangods-highleveldevices: 3.2.0
"""


@pytest.fixture
def fake_tango_ds(scope="session"):
    return yaml.safe_load(TANGO_DS)


@pytest.fixture
def fake_versions(scope="session"):
    return yaml.safe_load(VERSIONS)["versions"]
