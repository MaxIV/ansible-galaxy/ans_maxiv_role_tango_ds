import sys
import pytest

sys.path.insert(1, "filter_plugins")
import tango_ds


@pytest.mark.parametrize(
    "ds, expected",
    [
        (
            [{"name": "Foo"}],
            [],
        ),
        (
            [
                {
                    "name": "Foo",
                    "instances": [
                        {
                            "name": "test",
                        }
                    ],
                }
            ],
            [],
        ),
        (
            [
                {
                    "name": "Foo",
                    "instances": [
                        {
                            "name": "test",
                            "state": "absent",
                            "devices": [
                                {
                                    "name": "sys/test/foo",
                                },
                            ],
                        },
                        {
                            "name": "bar",
                            "level": 2,
                            "devices": [
                                {
                                    "name": "sys/test/bar",
                                },
                            ],
                        },
                    ],
                }
            ],
            [
                {
                    "device": "sys/test/bar",
                    "server": "Foo",
                    "instance": "bar",
                    "class": "Foo",
                },
            ],
        ),
        (
            [
                {
                    "name": "Foo",
                    "instances": [
                        {
                            "name": "test",
                            "devices": [
                                {
                                    "name": "sys/test/foo",
                                    "properties": {"hello": "world"},
                                },
                            ],
                        }
                    ],
                }
            ],
            [
                {
                    "device": "sys/test/foo",
                    "server": "Foo",
                    "instance": "test",
                    "class": "Foo",
                    "properties": {"hello": "world"},
                },
            ],
        ),
        (
            [
                {
                    "name": "Pool",
                    "instances": [
                        {
                            "name": "test",
                            "enabled": True,
                            "level": 5,
                            "devices": [
                                {"name": "sys/test/foo"},
                                {"name": "controller/test/bar", "class": "Controller"},
                            ],
                        }
                    ],
                }
            ],
            [
                {
                    "device": "sys/test/foo",
                    "server": "Pool",
                    "instance": "test",
                    "class": "Pool",
                },
                {
                    "device": "controller/test/bar",
                    "server": "Pool",
                    "instance": "test",
                    "class": "Controller",
                },
            ],
        ),
    ],
)
def test_tango_ds_devices(ds, expected):
    devices = tango_ds.tango_ds_devices(ds)
    assert devices == expected


def test_tango_ds_devices_fake(fake_tango_ds):
    devices = tango_ds.tango_ds_devices(fake_tango_ds)
    assert devices == [
        {
            "server": "TangoTest",
            "instance": "test",
            "class": "TangoTest",
            "device": "some-device/test/1",
            "properties": {"Baz": 42, "Foo": "Bar"},
        },
        {
            "server": "TangoTest",
            "instance": "test",
            "class": "TangoTest",
            "device": "some-device/test/2",
        },
        {
            "server": "Achtung",
            "instance": 1,
            "class": "Achtung",
            "device": "test/achtung/1",
        },
        {
            "server": "Achtung",
            "instance": 2,
            "class": "Achtung",
            "device": "test/achtung/2",
        },
        {
            "server": "Basler",
            "instance": "FE-SCRN-01",
            "class": "Basler",
            "device": "BASLER/FE-SCRN-01/main",
            "properties": {
                "camera_ip": "b303a-fe-dia-cam-01.maxiv.lu.se",
                "inter_packet_delay": "4000",
                "max_push_event_frequency": "2",
                "packet_size": "1500",
            },
        },
        {
            "server": "Basler",
            "instance": "O-SCRN-01",
            "class": "Basler",
            "device": "BASLER/O-SCRN-01/main",
            "properties": {
                "camera_ip": "b303a-o-dia-scrn-01.maxiv.lu.se",
                "inter_packet_delay": "4000",
                "max_push_event_frequency": "2",
                "packet_size": "1500",
            },
        },
    ]


@pytest.mark.parametrize(
    "ds, expected",
    [
        (
            [{"name": "Test", "conda_packages": {"package": "1.0.0"}}],
            [
                {
                    "name": "Test",
                    "conda_env_name": "Test",
                    "channels": [],
                    "conda_packages": {"package": "1.0.0"},
                    "conda_packages_list": [
                        "package=1.0.0",
                        "python=3.9",
                        "pytango=9.3.3",
                        "cpptango=9.3.5",
                    ],
                    "pip_packages_list": [],
                    "pypi_repo": "pypi-virtual-stable",
                    "servers": [],
                    "state": "present",
                },
            ],
        ),
        (
            [{"name": "Test", "conda_packages": {"package": "1.0.0", "python": "2.7"}}],
            [
                {
                    "name": "Test",
                    "conda_env_name": "Test",
                    "channels": [],
                    "conda_packages": {"package": "1.0.0", "python": "2.7"},
                    "conda_packages_list": [
                        "package=1.0.0",
                        "python=2.7",
                        "pytango=9.3.3",
                        "cpptango=9.3.5",
                    ],
                    "pip_packages_list": [],
                    "pypi_repo": "pypi-virtual-stable",
                    "servers": [],
                    "state": "present",
                },
            ],
        ),
        (
            [
                {
                    "name": "Foo",
                    "instances": [
                        {"name": "test1"},
                        {"name": "test2", "state": "absent"},
                    ],
                }
            ],
            [
                {
                    "name": "Foo",
                    "conda_env_name": "Foo",
                    "channels": [],
                    "conda_packages_list": [
                        "python=3.9",
                        "pytango=9.3.3",
                        "cpptango=9.3.5",
                    ],
                    "pip_packages_list": [],
                    "pypi_repo": "pypi-virtual-stable",
                    "servers": ["Foo/test1"],
                    "state": "present",
                },
            ],
        ),
        (
            [
                {
                    "name": "Foo",
                    "instances": [
                        {"name": "test1", "state": "absent"},
                        {"name": "test2", "state": "absent"},
                    ],
                }
            ],
            [
                {
                    "name": "Foo",
                    "conda_env_name": "Foo",
                    "channels": [],
                    "conda_packages_list": [
                        "python=3.9",
                        "pytango=9.3.3",
                        "cpptango=9.3.5",
                    ],
                    "pip_packages_list": [],
                    "pypi_repo": "pypi-virtual-stable",
                    "servers": [],
                    "state": "absent",
                },
            ],
        ),
        (
            [
                {
                    "name": "Foo",
                    "conda_env_name": "myenv",
                    "channels": ["extra"],
                    "conda_packages": {"package1": "1.0.0", "package2": "default"},
                    "pip_packages": {"foo": "default"},
                    "instances": [
                        {"name": "test1"},
                        {"name": "test2"},
                    ],
                }
            ],
            [
                {
                    "name": "Foo",
                    "conda_env_name": "myenv",
                    "channels": ["extra"],
                    "conda_packages": {"package1": "1.0.0", "package2": "default"},
                    "pip_packages": {"foo": "default"},
                    "conda_packages_list": [
                        "package1=1.0.0",
                        "package2=2.0.0",
                        "python=3.9",
                        "pytango=9.3.3",
                        "cpptango=9.3.5",
                    ],
                    "pip_packages_list": ["foo==1.2.1"],
                    "pypi_repo": "pypi-virtual-stable",
                    "servers": ["Foo/test1", "Foo/test2"],
                    "state": "present",
                },
            ],
        ),
        (
            [
                {
                    "name": "Foo",
                    "pip_packages": {"foo": "1.0.3.dev2"},
                    "pypi_repo": "pypi-virtual-test",
                    "instances": [
                        {"name": "test1"},
                    ],
                }
            ],
            [
                {
                    "name": "Foo",
                    "conda_env_name": "Foo",
                    "channels": [],
                    "pip_packages": {"foo": "1.0.3.dev2"},
                    "conda_packages_list": [
                        "python=3.9",
                        "pytango=9.3.3",
                        "cpptango=9.3.5",
                    ],
                    "pip_packages_list": ["foo==1.0.3.dev2"],
                    "pypi_repo": "pypi-virtual-test",
                    "servers": ["Foo/test1"],
                    "state": "present",
                },
            ],
        ),
        (
            [
                {
                    "name": "highleveldevices",
                    "conda_packages": {"tangods-highleveldevices": "default"},
                    "module_entry_points": "highleveldevices",
                }
            ],
            [
                {
                    "name": "highleveldevices",
                    "conda_env_name": "highleveldevices",
                    "module_entry_points": "highleveldevices",
                    "channels": [],
                    "conda_packages": {"tangods-highleveldevices": "default"},
                    "conda_packages_list": [
                        "tangods-highleveldevices=3.2.0",
                        "python=3.9",
                        "pytango=9.3.3",
                        "cpptango=9.3.5",
                    ],
                    "pip_packages_list": [],
                    "pypi_repo": "pypi-virtual-stable",
                    "servers": [],
                    "state": "present",
                }
            ],
        ),
        (
            [
                {
                    "name": "WaterPump",
                    "conda_packages": {"tangods-highleveldevices": "default"},
                    "instances": [
                        {"name": "test1"},
                        {"name": "test2"},
                    ],
                },
                {
                    "name": "VacuumGauge",
                    "conda_packages": {"tangods-highleveldevices": "default"},
                    "instances": [
                        {"name": "test1"},
                    ],
                },
            ],
            [
                {
                    "name": "WaterPump",
                    "conda_env_name": "WaterPump",
                    "channels": [],
                    "conda_packages": {"tangods-highleveldevices": "default"},
                    "conda_packages_list": [
                        "tangods-highleveldevices=3.2.0",
                        "python=3.9",
                        "pytango=9.3.3",
                        "cpptango=9.3.5",
                    ],
                    "pip_packages_list": [],
                    "pypi_repo": "pypi-virtual-stable",
                    "servers": ["WaterPump/test1", "WaterPump/test2"],
                    "state": "present",
                },
                {
                    "name": "VacuumGauge",
                    "conda_env_name": "VacuumGauge",
                    "channels": [],
                    "conda_packages": {"tangods-highleveldevices": "default"},
                    "conda_packages_list": [
                        "tangods-highleveldevices=3.2.0",
                        "python=3.9",
                        "pytango=9.3.3",
                        "cpptango=9.3.5",
                    ],
                    "pip_packages_list": [],
                    "pypi_repo": "pypi-virtual-stable",
                    "servers": ["VacuumGauge/test1"],
                    "state": "present",
                },
            ],
        ),
    ],
)
def test_tango_ds_conda_servers(fake_versions, ds, expected):
    servers = tango_ds.tango_ds_conda_servers(ds, fake_versions)
    assert servers == expected


def test_tango_ds_conda_servers_fake(fake_versions, fake_tango_ds):
    servers = tango_ds.tango_ds_conda_servers(fake_tango_ds, fake_versions)
    assert servers == [
        {
            "channels": ["maxiv-kits-dev"],
            "conda_env_name": "TangoTest",
            "conda_packages": {"tango-test": "default"},
            "conda_packages_list": [
                "tango-test=3.4",
                "python=3.9",
                "pytango=9.3.3",
                "cpptango=9.3.5",
            ],
            "name": "TangoTest",
            "pip_packages_list": [],
            "pypi_repo": "pypi-virtual-stable",
            "servers": ["TangoTest/test"],
            "state": "present",
        },
        {
            "channels": [],
            "conda_env_name": "achtung-env",
            "conda_packages_list": ["python=3.9", "pytango=9.3.3", "cpptango=9.3.5"],
            "name": "Achtung",
            "pip_packages": {"tangods-achtung": "0.1.5"},
            "pip_packages_list": ["tangods-achtung==0.1.5"],
            "pypi_repo": "pypi-virtual-stable",
            "servers": ["Achtung/1", "Achtung/2"],
            "state": "present",
        },
        {
            "channels": [],
            "conda_env_name": "Uview",
            "conda_packages_list": ["python=3.9", "pytango=9.3.3", "cpptango=9.3.5"],
            "name": "Uview",
            "pip_packages_list": [],
            "pypi_repo": "pypi-virtual-stable",
            "servers": [],
            "state": "absent",
        },
    ]


@pytest.mark.parametrize(
    "ds, expected",
    [
        (
            [
                {
                    "name": "Test",
                    "packages_stable": {"foo": "1.0.0"},
                }
            ],
            [
                {
                    "name": "Test",
                    "packages_stable": {"foo": "1.0.0"},
                    "packages_stable_present": ["foo-1.0.0"],
                    "packages_testing_present": [],
                    "servers": [],
                    "state": "present",
                },
            ],
        ),
        (
            [
                {
                    "name": "Foo",
                    "instances": [
                        {"name": "test1"},
                        {"name": "test2", "state": "absent"},
                    ],
                }
            ],
            [],
        ),
        (
            [
                {
                    "name": "Foo",
                    "packages_stable": {},
                    "instances": [
                        {"name": "test1"},
                        {"name": "test2", "state": "absent"},
                    ],
                }
            ],
            [
                {
                    "name": "Foo",
                    "packages_stable": {},
                    "packages_stable_present": [],
                    "packages_testing_present": [],
                    "servers": ["Foo/test1"],
                    "state": "present",
                },
            ],
        ),
        (
            [
                {
                    "name": "Foo",
                    "packages_stable": {"foo": "1.0.0"},
                    "instances": [
                        {"name": "test1", "state": "absent"},
                        {"name": "test2", "state": "absent"},
                    ],
                }
            ],
            [
                {
                    "name": "Foo",
                    "packages_stable": {"foo": "1.0.0"},
                    "packages_stable_present": ["foo-1.0.0"],
                    "packages_testing_present": [],
                    "servers": [],
                    "state": "absent",
                },
            ],
        ),
        (
            [
                {
                    "name": "Foo",
                    "packages_stable": {"foo": "default"},
                    "packages_testing": {"bar": "1.0.0.dev2"},
                    "instances": [
                        {"name": "test1"},
                        {"name": "test2"},
                    ],
                }
            ],
            [
                {
                    "name": "Foo",
                    "packages_stable": {"foo": "default"},
                    "packages_testing": {"bar": "1.0.0.dev2"},
                    "packages_stable_present": ["foo-1.2.1"],
                    "packages_testing_present": ["bar-1.0.0.dev2"],
                    "servers": ["Foo/test1", "Foo/test2"],
                    "state": "present",
                },
            ],
        ),
        (
            [
                {
                    "name": "WaterPump",
                    "packages_stable": {"tangods-highleveldevices": "default"},
                    "instances": [
                        {"name": "test1"},
                        {"name": "test2"},
                    ],
                },
                {
                    "name": "VacuumGauge",
                    "packages_stable": {"tangods-highleveldevices": "default"},
                    "instances": [
                        {"name": "test1"},
                    ],
                },
            ],
            [
                {
                    "name": "WaterPump",
                    "packages_stable": {"tangods-highleveldevices": "default"},
                    "packages_stable_present": ["tangods-highleveldevices-3.2.0"],
                    "packages_testing_present": [],
                    "servers": [
                        "VacuumGauge/test1",
                        "WaterPump/test1",
                        "WaterPump/test2",
                    ],
                    "state": "present",
                },
                {
                    "name": "VacuumGauge",
                    "packages_stable": {"tangods-highleveldevices": "default"},
                    "packages_stable_present": ["tangods-highleveldevices-3.2.0"],
                    "packages_testing_present": [],
                    "servers": [
                        "VacuumGauge/test1",
                        "WaterPump/test1",
                        "WaterPump/test2",
                    ],
                    "state": "present",
                },
            ],
        ),
    ],
)
def test_tango_ds_rpm_servers(fake_versions, ds, expected):
    servers = tango_ds.tango_ds_rpm_servers(ds, fake_versions)
    assert servers == expected


def test_tango_ds_rpm_servers_fake(fake_versions, fake_tango_ds):
    servers = tango_ds.tango_ds_rpm_servers(fake_tango_ds, fake_versions)
    assert servers == [
        {
            "name": "Basler",
            "packages_stable": {"tangods-basler": "default"},
            "packages_stable_present": ["tangods-basler-1.1.1"],
            "packages_testing_present": [],
            "servers": ["Basler/FE-SCRN-01"],
            "state": "present",
        },
    ]


@pytest.mark.parametrize(
    "ds, expected",
    [
        (
            [
                {
                    "name": "Foo",
                    "instances": [
                        {"name": "test1", "devices": []},
                    ],
                }
            ],
            [
                {
                    "server": "Foo/test1",
                    "state": "started",
                },
            ],
        ),
        (
            [
                {
                    "name": "Foo",
                    "instances": [
                        {"name": "test1", "level": 2},
                        {"name": "test2", "state": "absent"},
                    ],
                }
            ],
            [
                {
                    "server": "Foo/test1",
                    "state": "started",
                    "level": 2,
                },
                {
                    "server": "Foo/test2",
                    "state": "absent",
                },
            ],
        ),
    ],
)
def test_tango_ds_instances(ds, expected):
    instances = tango_ds.tango_ds_instances(ds)
    assert instances == expected


def test_tango_ds_instances_fake(fake_tango_ds):
    instances = tango_ds.tango_ds_instances(fake_tango_ds)
    assert instances == [
        {
            "server": "TangoTest/test",
            "state": "started",
        },
        {
            "server": "Achtung/1",
            "state": "started",
        },
        {
            "server": "Achtung/2",
            "state": "started",
        },
        {
            "server": "Basler/FE-SCRN-01",
            "state": "started",
        },
        {
            "server": "Basler/O-SCRN-01",
            "state": "present",
        },
        {
            "server": "Uview/BXXXA",
            "state": "absent",
        },
    ]


@pytest.mark.parametrize(
    "results, expected",
    [
        ([], []),
        (
            [
                {
                    "ds": {
                        "conda_env_name": "CosaxsPandabox",
                    },
                    "skip_reason": "Conditional result was False",
                    "skipped": True,
                },
                {
                    "ds": {
                        "conda_env_name": "highleveldevices",
                    },
                    "stdout_lines": [
                        "BeamShutter",
                        "CirculationFan",
                        "Compressor",
                    ],
                },
            ],
            [
                {"conda_env_name": "highleveldevices", "name": "BeamShutter"},
                {"conda_env_name": "highleveldevices", "name": "CirculationFan"},
                {"conda_env_name": "highleveldevices", "name": "Compressor"},
            ],
        ),
    ],
)
def test_tango_ds_get_entry_points(results, expected):
    eps = tango_ds.tango_ds_get_entry_points(results)
    assert eps == expected
