# ans_maxiv_role_tango_ds

Ansible role to deploy Tango Device Servers.

## Role Variables

Tango device servers are defined using a nested list of servers -> instances -> devices
as in the following example:

```yaml
tango_ds:
  - name: TangoTest
    conda_packages:
      tango-test: 3.4
    instances:
      - name: test
        devices:
          - name: some-device/test/1
            properties:
              Foo: Bar
              Baz: 42
          - name: some-device/test/2
  - name: Achtung
    conda_env_name: achtung-env
    pip_packages:
      tangods-achtung: default
    instances:
      - name: 1
        devices:
          - name: test/achtung/1
      - name: 2
        devices:
          - name: test/achtung/2
            class: Achtung
  - name: Basler
    packages_stable:
      tangods-basler: default
    instances:
      - name: FE-SCRN-01
        devices:
          - name: BASLER/FE-SCRN-01/main
      - name: O-SCRN-01
        state: present
        devices:
          - name: BASLER/O-SCRN-01/main
  - name: Uview
    instances:
      - name: BXXXA
        state: absent
  - name: highleveldevices
    conda_packages:
      tangods-highleveldevices: default
    module_entry_points: highleveldevices
```

Each server can be deployed using RPM or conda/pip.

* When using conda/pip, a specific conda environment is created and already includes `pytango`.
  Default versions of `python`, `pytango` and `cpptango` come from the `versions` dict but can be
  overwritten by specifying them in the `conda_packages` dict.
  If the `conda_env_name` isn't specified, the name of the server is used.
  Requirements are specified using the `conda_packages` and/or `pip_packages` dict variables.
* For RPM, packages are specified using the `packages_stable` or `packages_testing` dict variables.

All packages must be specified as key-value pairs of `name: version`. The version of the package should be explicit or set to `default`. In that case, the value defined in the `versions` dict will be used.

To install testing packages:

* For RPM, use the `packages_testing` variable:

  ```yaml
    - name: Basler
      packages_testing:
        tangods-basler: 1.0.1.dev3+gd9792c9
  ```

* For conda, add the `maxiv-kits-dev` channel to the `channels` variable (list of **extra channels** to pass):

  ```yaml
    - name: Basler
      conda_packages:
        tangods-basler: 1.0.1.dev3+gd9792c9
      channels:
        - maxiv-kits-dev
  ```

* For pip, set the `pypi_repo` variable to `pypi-virtual-test`:

  ```yaml
    - name: Basler
      pip_packages:
        tangods-basler: 1.0.1.dev3+gd9792c9
      pypi_repo: pypi-virtual-test
  ```

By default, each instance state is set to `started`, meaning the server will be started/restarted by starter if required.
The `state` can be set to `present` so that the packages are installed but the server isn't touched (neither started nor restarted).
If `state` is set to `absent`, the tango device server will be stopped.
If deployed with conda, the environment is also deleted.
RPM aren't removed as they are installed globally and could be used by something else.

`level` specify the startup level of the device. Won't be modified if not specified

Each defined device will be registered in the database. If the `class` isn't specified, the name of the server is used. Devices are never deleted.

A tango device server usually contains one entry point (the executable to start),
which matches the name of the class.
For conda packages, a wrapper is automatically created under `/usr/local/bin`.
Some packages, like `tangods-highleveldevices`, can contain many entry points.
In that case, the variable `module_entry_points` can be set to the module name
to automatically discover all entry points and create wrappers for them:

```yaml
  - name: highleveldevices
    conda_packages:
      tangods-highleveldevices: default
    module_entry_points: highleveldevices
```

See the full list of variables in [defaults/main.yml](defaults/main.yml)

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ans_maxiv_role_tango_ds
```

## License

GPL-3.0-or-later
